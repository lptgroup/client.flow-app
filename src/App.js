import React, {Component} from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import './App.css';
import './styles/flexboxgrid.min.css';
import Content from './containers/Content';
import Header from './containers/Header';
import SignUp from './containers/SignUp';
import Error from './containers/Error';
import AdminLogin from './containers/AdminLogin';
import AdminDashboard from './containers/AdminDashboard';
import AdminUserAnswer from './containers/AdminUserAnswer';
import Variables from './config/.env';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/genie.css';
import {connect} from 'react-redux';
class App extends Component {
  render() {
    if (this.props.token.isFetching || this.props.token.id === Variables().token) {
      return (
        <Router>
          <div>
            {(this.props.token.id === Variables().token)
              ? <Header/>
              : <Route component={Error}/>
}
            <div className="container">
              <Route exact path="/:token?" component={Content}/>
              <Route path="/signUp/:token" component={SignUp}/>
              <Route path="/admin/login" component={AdminLogin}/>
              <Route path="/admin/dashboard" component={AdminDashboard}/>
              <Route path="/admin/answer/:id" component={AdminUserAnswer}/>
            </div>
          </div>
        </Router>
      );
    } else if (this.props.token.type === 'admin') {
      return (
        <Router>
          <div className="container">
            <Route path="/admin/login" component={AdminLogin}/>
            <Route path="/admin/dashboard" component={AdminDashboard}/>
            <Route path="/admin/answer/:id" component={AdminUserAnswer}/>
          </div>
        </Router>
      )
    } else {
      return (<Error/>)
    }

  }
}
function mapStateToProps(state, ownProps) {
  return {token: state.token};
}
export default connect(mapStateToProps)(App);
