import axios from 'axios';
import Variables from '../config/.env';

const BASE_URL = Variables().BASE_URL;
export const GetSingleAnswer = (id) => {
  const url = `${BASE_URL}/${Variables().token}/answers/${id}`;
  return axios
    .get(url)
    .then(response => response);
};
export const GetAllQuestions = (id) => {
  const url = `${BASE_URL}/${Variables().token}/questions`;
  return axios
    .get(url)
    .then(response => response);
};
export const createUser = (data) => {
  const url = `${BASE_URL}/${Variables().token}/signUp`;
  return axios
    .post(url, data)
    .then(response => response);
};
export const addAnswer = (data) => {
  const url = `${BASE_URL}/${Variables().token}/userAnswer`;
  return axios
    .post(url, data)
    .then(response => response);
};
export const login = (credentials) => {
  const url = `${BASE_URL}/auth/login`;
  return axios
    .post(url, credentials)
    .then(response => response);
};

export const GetUsers = () => {
  const url = `${BASE_URL}/users`;
  const CONFIG = {
    headers: {'Authorization': "bearer" + localStorage.getItem('id_token')}
  };
  return axios
    .get(url, CONFIG)
    .then(response => response);
};
export const GetUserAnswers = (id) => {
  const url = `${BASE_URL}/answerSents/${id}`;
  const CONFIG = {
    headers: {'Authorization': "bearer" + localStorage.getItem('id_token')}
  };
  return axios
    .get(url, CONFIG)
    .then(response => response);
};