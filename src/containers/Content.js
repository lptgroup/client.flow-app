import React, {Component} from 'react';
import '../styles/Content.css';
import {GetSingleAnswer, GetAllQuestions} from '../api/rest-api';
import Radio from '../components/Radio';
import {Question} from '../components/Questions';
import {QuestionsList} from '../components/QuestionsList';
import Loader from '../components/Loader'
import {Button} from 'react-materialize';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import * as contentActions from '../actions/contentActions';
import * as tokenActions from '../actions/tokenActions';
import Variables from '../config/.env';
class Content extends Component {
  static initialState = () => ({
    answer: {
      dynamo: {},
      blaze: {},
      tempo: {},
      steel: {}
    },
    question: '',
    questions: [],
    currentQuestion: 1,
    answerToSend: {
      question_id: '',
      answer_id: '',
      user_id: ''
    },
    selectedOption: '',
    storeAllAnswers: []
  });
  state = Content.initialState();
  getSingleAnswer = id => (GetSingleAnswer(id).then((result) => {
    this.setAnswers(result.data.answers);
    this.setState(() => {
      return {question: result.data.question};
    });
  }))
  setAnswers(answers) {
    const answer = this.state.answer;
    answers.map(result => {
      if (result.type_id === 1) {
        return answer.dynamo = result;
      } else if (result.type_id === 2) {
        return answer.blaze = result;
      } else if (result.type_id === 3) {
        return (answer.tempo = result)
      } else {
        return answer.steel = result;
      }
    })
  }
  questionIncrementer = () => {
    this.setState(prevState => ({
      currentQuestion: prevState.currentQuestion + 1
    }), () => this.getSingleAnswer(this.state.currentQuestion))

  }
  setNextAnswerValue() {
    const answerToSend = this.state.answerToSend;
    answerToSend.question_id = this.state.question.id;
    this
      .props
      .actions
      .addAnswer(answerToSend);
  }
  handleOptionChange = (changeEvent) => {
    const answerToSend = this.state.answerToSend;
    const selectedValue = changeEvent.target.value
    if (selectedValue === 'dynamo') {
      answerToSend.answer_id = 1;
    } else if (selectedValue === 'blaze') {
      answerToSend.answer_id = 2;
    } else if (selectedValue === 'tempo') {
      answerToSend.answer_id = 3;
    } else {
      answerToSend.answer_id = 4;
    }
    this.setState({answerToSend, selectedOption: changeEvent.target.value});
  }
  resetAnswerToSend = () => this.setState({
    answerToSend: Content
      .initialState()
      .answerToSend
  });
  resetSelectedOption = () => this.setState({selectedOption: ''});
  redirectToSaveUser() {
    if (this.state.currentQuestion === this.state.questions.length) {
      this
        .props
        .history
        .push(`signUp/${Variables().token}`)
    }

  }
  componentWillMount() {
    this.props.match.params && this.props.match.params.token === Variables().token
      ? this
        .props
        .actions
        .setToken({id: this.props.match.params.token, isFetching: false, type: 'user'})
      : this
        .props
        .actions
        .setToken({id: '', isFetching: false});
  }
  componentDidMount() {
    if (this.props.match.params && this.props.match.params.token !== Variables().token) {
      return;
    }
    this.getSingleAnswer(this.state.currentQuestion);
    GetAllQuestions().then((result) => {
      this.setState(() => {
        return {questions: result.data, maxQuestion: result.data.length};
      });
    })
  }
  render() {
    const {answer, question, questions, currentQuestion, selectedOption} = this.state;
    return (
      <div className="row">
        <div className="col-md-12 col-lg-6 col">
          <div className="box">
            <Question text={question.title}/>
            <div className="col answers">
              <div className="radio">
                <Radio
                  label={answer.dynamo.answer}
                  type="dynamo"
                  check={selectedOption === 'dynamo'}
                  change={this.handleOptionChange}/>
              </div>
              <div className="radio">
                <Radio
                  type="blaze"
                  check={selectedOption === 'blaze'}
                  change={this
                  .handleOptionChange}
                  label={answer.blaze.answer}/>
              </div>
              <div className="radio">
                <Radio
                  type="tempo"
                  check={selectedOption === 'tempo'}
                  change={this
                  .handleOptionChange}
                  label={answer.tempo.answer}/>
              </div>
              <div className="radio">
                <Radio
                  type="steel"
                  check={selectedOption === 'steel'}
                  change={this
                  .handleOptionChange}
                  label={answer.steel.answer}/>
              </div>
              <div className="radio answers__button">
                <Button
                  className="content__button btn--main"
                  waves='light'
                  onClick=
                  { () => { this.questionIncrementer(); this.setNextAnswerValue(); this.resetAnswerToSend(); this.resetSelectedOption(); this.redirectToSaveUser()}}
                  disabled={!selectedOption}>
                  Następne pytanie
                </Button>
              </div>
            </div>
          </div>
        </div>
        <div className="col-xs-12 col-lg-6">
          <div className="box">
            {questions.length
              ? <QuestionsList currentQuestion={currentQuestion} questions={questions}/>
              : <Loader/>}
          </div>
        </div>
      </div>
    );
  }
}
function mapStateToProps(state, ownProps) {
  return {answers: state.answers};
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Object.assign({}, contentActions, tokenActions), dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Content);
