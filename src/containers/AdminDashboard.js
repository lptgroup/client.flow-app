import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Redirect } from 'react-router'
import {bindActionCreators} from 'redux'
import * as loginActions from '../actions/loginActions';
import * as tokenActions from '../actions/tokenActions';
import {GetUsers} from '../api/rest-api';
import {UsersList} from '../components/UsersList';
import Loader from '../components/Loader'
import AdminLogout from './AdminLogout'
class AdminDashboard  extends Component {
    state = { users: [] }
    getUsers = id => (GetUsers(id).then((result) => {
      if (this.refs.loaded) {
      this.setState({users: result.data});
    }
    }))
    componentWillMount() {
      
      this.getUsers();
    
      this
        .props
        .actions
        .setToken({id: '', isFetching: false, type: 'admin'})
    }
    componentDidMount() {
      
    }
    render() {
      const {users} = this.state;
        return (
            (this.props.auth.isAuthenticated) ?
            <div ref="loaded" className="box">
            {users.length
              ?
              <div className="p-relative center-xs">
              <h2>Lista uczestników</h2> 
              <AdminLogout />
              <UsersList users={users}/>
              </div>
              : <Loader/>}
          </div> :
            <Redirect to="/admin/login"/>
        );
    }
}
function mapStateToProps(state, ownProps) {
  return {auth: state.login};
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Object.assign({}, loginActions, tokenActions), dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(AdminDashboard);
