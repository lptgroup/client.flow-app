import React, {Component} from 'react';
import {LogoutButton} from '../components/LogoutButton';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import * as logoutActions from '../actions/logoutActions';
class AdminLogout extends Component {
  state = {}
  logout = () => {
    this
      .props
      .actions
      .requestLogout();
    localStorage.removeItem('id_token');
    this
      .props
      .actions
      .receiveLogout();
      this.backToLogin()
  }
  backToLogin = () => {
    debugger;
    console.log(this)
    this._reactInternalInstance._context.router
    .history
    .push('/admin/login')
  }
  render() {
    return (<LogoutButton logout={this.logout}/>);
  }
}
function mapStateToProps(state, ownProps) {
  return {};
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(logoutActions, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(AdminLogout);
