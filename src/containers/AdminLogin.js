import React, {Component} from 'react';
import {Row, Input, Button} from 'react-materialize';
import {login} from '../api/rest-api';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import * as loginActions from '../actions/loginActions';
import * as tokenActions from '../actions/tokenActions';
class AdminLogin extends Component {
  static initialState = () => ({
    user: {
      email: '',
      password: ''
    }
  })
  state = AdminLogin.initialState();
  handleChange = (event) => {
    event.preventDefault();
    const user = this.state.user;
    user[event.target.name] = event.target.value
    this.setState({user})
  }
  login = event => {
    event.preventDefault();
    const self = this;
    this
      .props
      .actions
      .requestLogin(this.state.user);
    return login(this.state.user).then((result) => {
      localStorage.setItem('id_token', result.data.token);
      self
        .props
        .actions
        .receiveLogin(result.data.token);
     
    }).then(()=>{
      this
      .props
      .history
      .push('/admin/dashboard')
    }).catch(error => {
      self
        .props
        .actions
        .loginError(error);
      throw(error);
    })
  }
  componentWillMount() {
    this
      .props
      .actions
      .setToken({id: '', isFetching: false, type: 'admin'})
  }
  render() {
    const {user} = this.state;
    return (
      <div>
        <div className="center-xs">
          <h2 >Zaloguj się do panelu admina</h2>
        </div>
        <form onSubmit={this.login}>
          <Row>
            <Input
              type="email"
              label="Email"
              name="email"
              value={user.email}
              onChange={this.handleChange}
              s={12}
              required/>
            <Input
              type="password"
              label="Hasło"
              name="password"
              value={user.password}
              onChange={this.handleChange}
              s={12}
              required/>

            <Button type="submit" className="content__button btn--main" waves='light'>Zaloguj się</Button>
          </Row>
        </form>
      </div>
    );
  }
}
function mapStateToProps(state, ownProps) {
  return {};
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Object.assign({}, loginActions, tokenActions), dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(AdminLogin);
