import React, {Component} from 'react';
import {Navbar} from 'react-materialize';
import Variables from '../config/.env';
class Header extends Component {
  state = {}
  render() {
    return (
      <Navbar href={`/${Variables().token}`} brand='Sprawdź swoje flow' right></Navbar>
    );
  }
}

export default Header;
