import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router'
import {bindActionCreators} from 'redux'
import * as loginActions from '../actions/loginActions';
import * as tokenActions from '../actions/tokenActions';
import {GetUserAnswers} from '../api/rest-api';
import {UserAnswers} from '../components/UserAnswers';
import {UserAnswersName} from '../components/UserAnswersName';
import {DownloadButton} from '../components/DownloadButton';
import {RadarChart} from '../components/RadarChart';
import Loader from '../components/Loader';
import pdfConverter from 'jspdf';
import html2canvas from 'html2canvas';
import '../styles/Admin.css';
class AdminUserAnswers extends Component {
    state = {
        answersPercentage: {
            dynamo: 0,
            blaze: 0,
            tempo: 0,
            steel: 0
        },
        talents: {
            creator: 0,
            star: 0,
            supporter: 0,
            dealMaker: 0,
            trader: 0,
            accumulator: 0,
            lord: 0,
            mechanic: 0
        },
        talentsArray: [],
        user: {},
        showLoader: true
    }
    getUserAnswers = id => (GetUserAnswers(id).then((result) => {
        if (this.refs.loaded) {
            this.getAnswersByTalents(result.data);
            this.setState({user: result.data[0].user});
        }
    }))
    getAnswersByTalents(answers) {
        const answersPercentage = this.state.answersPercentage;
        const answerCount = answers.length;
        answers.map(answer => {
            if (answer.answer.type_id === 1) {
                return answersPercentage.dynamo += 1;
            } else if (answer.answer.type_id === 2) {
                return answersPercentage.blaze += 1;
            } else if (answer.answer.type_id === 3) {
                return answersPercentage.tempo += 1;
            } else {
                return answersPercentage.steel += 1;
            }
        })

        this.setPercentageAnswers(answersPercentage, answerCount);
    }
    setPercentageAnswers(answersPercentage, answerCount) {
        for (let answer in answersPercentage) {
            answersPercentage[answer] = Math.round(answersPercentage[answer] / answerCount * 100);
        }
        this.setState({answersPercentage});
        this.setTalents(answersPercentage);
    }
    setTalents(answersPercentage) {
        const multiplier = 1.5,
            talents = this.state.talents;
        talents.creator = answersPercentage.dynamo;
        talents.star = Math.round((answersPercentage.dynamo + answersPercentage.blaze) / multiplier);
        talents.supporter = answersPercentage.blaze;
        talents.dealMaker = Math.round((answersPercentage.blaze + answersPercentage.tempo) / multiplier);
        talents.trader = answersPercentage.tempo;
        talents.accumulator = Math.round((answersPercentage.tempo + answersPercentage.steel) / multiplier);
        talents.lord = answersPercentage.steel;
        talents.mechanic = Math.round((answersPercentage.steel + answersPercentage.dynamo) / multiplier);
        this.setState({talents});
        this.setTalentsArray(talents)
    }
    setTalentsArray(talents) {
        let talentsArray = this
            .state
            .talentsArray
            .slice()

        for (let key in talents) {
            talentsArray.push(talents[key])
        }
        this.setState({talentsArray})
        this.setState({showLoader: false})
    }
    startPrintProcess = (canvasObj, fileName, callback) => {
        window.html2canvas = html2canvas;
        let pdf = new pdfConverter('p', 'pt', 'a4'),
            pdfConf = {
                pagesplit: true,
                background: '#fff'
            },
            state = this.state;
        document
            .body
            .appendChild(canvasObj);
        pdf.addHTML(canvasObj, 0, 10, pdfConf, function () {
            document
                .body
                .removeChild(canvasObj);
            pdf.save(`${state.user.name}_${state.user.surname}.pdf`);
            callback();
        });
    }
    pdfToHTML = () => {
        this.setState({showLoader: true})
        let source = document.querySelector('#js-to-pdf'),
        canvas = source.querySelector('canvas');
        source.style.padding = '0 30px';
        canvas.style.padding = '0 60px 0 0';

        let self = this;
        html2canvas(source, {
            onrendered: function (canvasObj) {
                self
                    .startPrintProcess(canvasObj, 'printedPDF', function () {
                        self.setState({showLoader: false})
                        source.style.padding = ''
                        canvas.style.padding = '';
                    });
                //save this object to the pdf
            }
        });
    }
    componentWillMount() {
        this
            .props
            .actions
            .setToken({id: '', isFetching: false, type: 'admin'})
    }
    componentDidMount() {
        const userId = this.props.match.params.id
        this.getUserAnswers(userId);

    }
    render() {
        const {answersPercentage, talentsArray, user, showLoader} = this.state;
        return ((this.props.auth.isAuthenticated)
            ? <div ref="loaded" className="center-xs">
                    {talentsArray.length > 0 && <div className="p-relative" id="js-to-pdf">
                        <UserAnswersName user={user}/>
                        <UserAnswers answers={answersPercentage}/><br/>
                        <RadarChart talents={talentsArray}/>
                        <DownloadButton download={this.pdfToHTML}/>
                    </div>}
                    {showLoader && <Loader/>}
                </div>
            : <Redirect to="/admin/login"/>);
    }
}
function mapStateToProps(state, ownProps) {
    return {auth: state.login};
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Object.assign({}, loginActions, tokenActions), dispatch)
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AdminUserAnswers);