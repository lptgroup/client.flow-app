import React, {Component} from 'react';
import {Navbar} from 'react-materialize';
class AdminHeader extends Component {
  state = {}
  render() {
    return (
      <Navbar href="/admin/dashboard" brand='Panel administracyjny' right></Navbar>
    );
  }
}

export default AdminHeader;
