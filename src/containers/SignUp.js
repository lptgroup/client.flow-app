import React, {Component} from 'react';
import {Row, Input, Button} from 'react-materialize';
import {createUser, addAnswer} from '../api/rest-api';
import Alert from 'react-s-alert';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux'
import * as tokenActions from '../actions/tokenActions';
import Variables from '../config/.env';
class SignUp extends Component {
  static initialState = () => ({
    user: {
      name: '',
      surname: '',
      email: ''
    }
  })
  state = SignUp.initialState();
  createUser = event => {
    event.preventDefault();
    createUser(this.state.user).then((result) => {
      this
        .props
        .answers
        .map(answer => {
          return answer.user_id = result.data.userId
        })
      this.addAnswer(this.props.answers)
    })
  }
  addAnswer = data => {
    this.clearForm();
    addAnswer(data).then((result) => {
      this.handleSuccess()
    }).catch(() => {
      this.handleError()
    })

  }
  handleChange = (event) => {
    event.preventDefault()
    const user = this.state.user;
    user[event.target.name] = event.target.value
    this.setState({user})
  }
  handleSuccess() {
    Alert.success('Gratulacje! twoje odpowiedzi zostały wysłane', {
      position: 'top-right',
      effect: 'genie'
    });
  }
  handleError() {
    Alert.error('Wystąpił problem z wysłaniem twoich odpowiedzi. Spróbuj ponownie', {
      position: 'top-right',
      effect: 'genie'
    });
  }
  clearForm = () => {
    document
      .getElementById("js-form")
      .reset();
  }
  clearForm = () => this.setState({
    user: SignUp
      .initialState()
      .user
  });
  componentWillMount() {
    this.props.match.params && this.props.match.params.token === Variables().token
    ? this
      .props
      .actions
      .setToken({id:this.props.match.params.token, isFetching:false, type: 'user'})
    : this
      .props
      .actions
      .setToken({id:'', isFetching:false});
  }
  render() {
    const {user} = this.state;
    return (
      <div>
        <div className="center-xs">
          <h2 >Wyślij swój wynik</h2>
        </div>
        <form onSubmit={this.createUser}>
          <Row>
            <Input
              type="text"
              label="Imię"
              name="name"
              value={user.name}
              onChange={this.handleChange}
              s={12}
              required/>
            <Input
              type="text"
              label="Nazwisko"
              name="surname"
              value={user.surname}
              onChange={this.handleChange}
              s={12}
              required/>
            <Input
              type="email"
              label="Email"
              name="email"
              value={user.email}
              onChange={this.handleChange}
              s={12}
              required/>
            <Button type="submit" className="content__button btn--main" waves='light'>Zapisz swój wynik</Button>
          </Row>
        </form>
        <Alert stack={{
          limit: 3
        }}/>
      </div>
    );
  }
}
function mapStateToProps(state, ownProps) {
  return {answers: state.answers};
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(tokenActions,  dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
