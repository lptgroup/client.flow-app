import * as types from '../actions/actionTypes';
export default function tokenReducer(state = {isFetching: true, id: '', type: ''}, action) {
  switch (action.type) {
    case types.SET_TOKEN:
      return  Object.assign({}, state, {
        isFetching: action.token.isFetching,
        id: action.token.id,
        type: action.token.type
      });
    default:
      return state;
  }
}
