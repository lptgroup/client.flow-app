import * as types from '../actions/actionTypes';
export default function contentReducer(state = [], action) {
  switch (action.type) {
    case types.ADD_ANSWER:
      return [
        ...state,
        Object.assign({}, action.answer)
      ];

    default:
      return state;
  }
}
