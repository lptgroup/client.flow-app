import * as types from '../actions/actionTypes';
export default function logoutReducer (state = {
    isFetching: false,
    isAuthenticated: localStorage.getItem('id_token') ? true : false
  }, action) {
  switch (action.type) {
    case types.LOGOUT_SUCCESS:
    return Object.assign({}, state, {
      isFetching: true,
      isAuthenticated: false
    });
    default:
      return state;
  }
}
