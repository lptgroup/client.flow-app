import {combineReducers} from 'redux';
import answers from './contentReducer';
import token from './tokenReducer';
import login from './loginReducer';
import logout from './logoutReducers';

const rootReducer = combineReducers({
    answers, token, login, logout
});

export default rootReducer;