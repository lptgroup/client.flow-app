import React from 'react';

const UserAnswersName = (props) => {
    return (
        <h2>{`${props.user.name} ${props.user.surname}`}</h2>
    );
}

export {UserAnswersName};