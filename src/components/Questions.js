import React from 'react';
import PropTypes from 'prop-types';

const Question = props => (
  <h4>{props.text}</h4>
);

Question.propTypes = {
  text: PropTypes.string
};
export {Question};
