import React from 'react';
import {Button} from 'react-materialize';
import PropTypes from 'prop-types';
const DownloadButton = props => (<Button
    onClick={props.download}
    floating
    large
    data-html2canvas-ignore
    className="red admin__btn--logout"
    waves="light"
    icon="file_download"/>);

DownloadButton.propTypes = {
    download: PropTypes.func
};
export {DownloadButton};
