import React from 'react';
import {Button} from 'react-materialize';
import PropTypes from 'prop-types';
const LogoutButton = props => (<Button
    onClick={props.logout}
    floating
    large
    className="red admin__btn--logout"
    waves="light"
    icon="exit_to_app"/>);

LogoutButton.propTypes = {
    logout: PropTypes.func
};
export {LogoutButton};
