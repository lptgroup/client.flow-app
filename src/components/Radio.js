import React from 'react';
import {Input} from 'react-materialize';
import PropTypes from 'prop-types';

const Radio = props => (<Input
    name="group1"
    type="radio"
    label={props.label}
    value={props.type}
    checked={props.check}
    onChange={props.change}
    className="with-gap text--lg"/>);

Radio.propTypes = {
    label: PropTypes.string,
    type: PropTypes.string,
    check: PropTypes.bool,
    change: PropTypes.func
};
export default Radio;
