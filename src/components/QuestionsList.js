import React from 'react';
import PropTypes from 'prop-types';

const QuestionsList = (props) => {
  let listTextClass;
  return (
    <ol>
      {props
        .questions
        .map((question, index) => {
          if (index === props.currentQuestion - 1) {
            listTextClass = "list__text--current";
          } else if (index > props.currentQuestion - 1) {
            listTextClass = "list__text--answered";
          } else {
            listTextClass = "list__text--active";
          }
          return <li key={index} className={"list__text " + listTextClass}>{question.title}</li>;
        })}
    </ol>
  );
};

QuestionsList.propTypes = {
  questions: PropTypes.array,
  currentQuestion: PropTypes.number
};
export {QuestionsList};