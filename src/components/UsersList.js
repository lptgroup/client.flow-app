import React from 'react';
import {Collection, CollectionItem} from 'react-materialize';
import PropTypes from 'prop-types';

const UsersList = (props) => {
  return (
    <Collection>
      {props
        .users
        .map((user, index) => {
          return <CollectionItem href={`/admin/answer/${user.id}`} key={index}>{`${user.name} ${user.surname}`}</CollectionItem>;
        })}
    </Collection>
  );

};

UsersList.propTypes = {
  users: PropTypes.array
};
export {UsersList};