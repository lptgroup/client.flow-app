import React from 'react';
import {Radar} from 'react-chartjs-2';
import PropTypes from 'prop-types';

const RadarChart = props => {
    const talents = props.talents;
    const scaleMaxValue = Math.max(...talents) + 5;
    const data = {
        labels: [
            'Creator',
            'Star',
            'Supporter',
            'Deal meaker',
            'Trader',
            'Accumulator',
            'Lord',
            'Mechanic'
        ],

        datasets: [
            {
                backgroundColor: 'rgba(179,181,198,0.2)',
                borderColor: 'rgba(179,181,198,1)',
                pointBackgroundColor: 'rgba(179,181,198,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(179,181,198,1)',
                data: talents,
                options: {
                    scales: {
                        yAxes: [
                            {
                                ticks: {
                                    beginAtZero: true
                                }
                            }
                        ]
                    }
                }
            }, {
                backgroundColor: 'rgba(179,181,198,0)',
                borderColor: 'rgba(179,181,198,0)',
                pointBackgroundColor: 'rgba(179,181,198,0)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(179,181,198,0)',

                data: [0, scaleMaxValue]
            }
        ]

    };
    const radarOptions = {
        scale: {
            pointLabels: {
                fontSize: 14
            }
        },
        responsive: true,
        maintainAspectRatio: true,
        title: {
            display: false
        },
        legend: {
            display: false
        }
    };

    return (<Radar data={data} options={radarOptions}/>);
};
RadarChart.propTypes = {
    talents: PropTypes.array
};
export {RadarChart};