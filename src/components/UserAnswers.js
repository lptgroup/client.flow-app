import React from 'react';
import PropTypes from 'prop-types';

const UserAnswers = (props) => {

    return (
        <div  className="row center-xs user-answer__wrapper">
            <div className="user-answer__item col-xs-3">Dynamo: {props.answers.dynamo}%</div>
            <div className="user-answer__item col-xs-3">Blaze: {props.answers.blaze}%</div>
            <div className="user-answer__item col-xs-3">Tempo: {props.answers.tempo}%</div>
            <div className="user-answer__item col-xs-3">Steel: {props.answers.steel}%</div>
        </div>
    );
};

UserAnswers.propTypes = {
    answers: PropTypes.object
};
export {UserAnswers};