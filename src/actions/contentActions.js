import * as types from './actionTypes';
export function addAnswer(answer) {
    return { type: types.ADD_ANSWER, answer};
}
