import * as types from './actionTypes';
export function setToken(token) {
    return { type: types.SET_TOKEN,  token};
}